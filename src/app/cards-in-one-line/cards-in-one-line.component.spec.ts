import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardsInOneLineComponent } from './cards-in-one-line.component';

describe('CardsInOneLineComponent', () => {
  let component: CardsInOneLineComponent;
  let fixture: ComponentFixture<CardsInOneLineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardsInOneLineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardsInOneLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
